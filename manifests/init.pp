class apt_repositories {

   file { '/etc/apt/trusted.gpg.d/futurice.gpg':
      ensure => 'file', 
      owner => 'root', 
      group => 'root', 
      mode => '0644',
      source => 'puppet:///modules/apt_repositories/etc/apt/trusted.gpg.d/futurice.gpg',
      notify => Exec['apt-update'],
   }
 
   exec { 'apt-update':
      command     => '/usr/bin/apt-get update',
      refreshonly => true,
   }

}
